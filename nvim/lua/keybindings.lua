-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap=true, silent=true }
vim.api.nvim_set_keymap('n', '<space>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
vim.api.nvim_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
vim.api.nvim_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
vim.api.nvim_set_keymap('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)

local set = vim.keymap.set

-- 0 key takes to first char in line. if already on first char then takes beginning of line
set({'n','x','v'}, '0', function()
    if vim.fn.virtcol('.') == vim.fn.indent('.')+1
    then
       return '0'
    end
    return '^'
end, { expr=true,noremap=true})

set({ "i", "n" }, "<esc>", "<cmd>noh<cr><esc>", { desc = "Clear hlsearch and ESC" })
-- paste over currently selected text without yanking it
set("v", "p", '"_dp')
set("v", "P", '"_dP')

set({'n','x'}, '<F12>', ':Savesess<cr>',{ noremap=true})
--set('n', '<leader>a', ':Ag! <c-r><c-w><cr>',opts)
set('n', '<leader>e', ':b#<cr>',opts)
set('n', '<leader>b', ':Buffers<cr>',opts)
set('n', '<leader>w', '<Esc>:w<cr>',opts)
set('n', '<leader>x', '<Esc>:x<cr>',opts)
set('n', '<leader>qq', ':q!<cr>',opts)
set('n', '<leader>qa', ':qa!<cr>',opts)
set('n', '<Space>e', ':NvimTreeToggle<cr>',opts)
set('n', '<leader>f', ':BLines<cr>',opts)
set('v', '<leader>y', '"+y',opts)
set('n', '<leader>o', '<Esc>o<Esc>',opts)
set('n', '<leader>O', '<Esc>O<Esc>',opts)
set('n', '<leader>t', ':ToggleTerm direction=float<CR>',opts)
set('n', 'tn', ':tabn<cr>',opts)
set('n', 'tp', ':tabp<cr>',opts)
set('n', ':', ';',{noremap=true})
set('n', ';', ':',{noremap=true})
set('n', '<bar>', ':vsp<cr>:b#,<cr>',opts)
set('n', '<c-p>', ':Files<cr>',opts)
set('n','<leaderjs', ':!jq . %', opts)
set('n', '<leader>i', ':Vselquote i<cr>', opts)
set('n', '<leader>a', ':Vselquote a<cr>', opts)

function _G.set_terminal_keymaps()
  local o = {buffer = 0}
  -- vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], o)
  vim.keymap.set('t', 'jk', [[<C-\><C-n>]], o)
  vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], o)
  vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], o)
  vim.keymap.set('t', '<C-k>', [[<Cmd>wincmd k<CR>]], o)
  vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], o)
  vim.keymap.set('t', '<C-w>', [[<C-\><C-n><C-w>]], o)
end

-- if you only want these mappings for toggle term use term://*toggleterm#* instead
vim.cmd('autocmd! TermOpen term://*toggleterm#* lua set_terminal_keymaps()')

vim.api.nvim_create_user_command('Savesess',
function()
    local sess = vim.v.this_session
    if (sess == "") then
        sess = vim.fn.input("Enter session name: ", ".vims")
    end

    if (sess ~= "") then
        vim.cmd("mks! " .. sess)
    end
end,{nargs=0})

-- select string in/arrount single or double quotes
vim.api.nvim_create_user_command('Vselquote',
function(tbl)
    local ll = vim.api.nvim_get_current_line()
    local a = string.find(ll, "\"")
    local b = string.find(ll, "'")

    if a ~= nil and b ~= nil then
        if a < b then
            vim.cmd.normal("v" .. tbl.args .. "\"")
        else
            vim.cmd.normal("v" .. tbl.args .. "i'")
        end
    elseif a ~= nil then
        vim.cmd.normal("v" .. tbl.args .."\"")
    elseif b ~= nil then
        vim.cmd.normal("v" .. tbl.args .."'")
    end
end,{nargs=1}
)
