local g = vim.g
g.mapleader=","
g.ackprg =  'ag --nogroup --nocolor --column --ignore-dir vendor --ignore-dir node_modules'
g.grepprg =  'ag --nogroup --nocolor --column --ignore-dir vendor --ignore-dir node_modules'
g.sonokai_style = 'andromeda'

local o = vim.o
--vim.o.ruler = true
o.number = true
o.relativenumber = true
o.tabstop = 4
o.shiftwidth = 4
o.expandtab = true
o.ignorecase = true
o.hidden = true
o.autoindent = true
--o.laststatus = 2
o.diffopt='vertical'
--o.list = true
--o.statusline = '%-3.10n%f%=%l:%c'

