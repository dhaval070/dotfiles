Packer = require 'packer'

local use = Packer.use
Packer.startup(function()
    use 'christoomey/vim-tmux-navigator'
    use 'mhinz/vim-startify'
    use 'jghauser/mkdir.nvim'
    use { 'michaelb/sniprun', run = 'sh ./install.sh'}
    use 'tpope/vim-repeat'
    use'sainnhe/sonokai'
    use {
        'nvim-lualine/lualine.nvim',
        --requires = { 'kyazdani42/nvim-web-devicons', opt = true },
        config = function()
            require('plugins/lualine')
        end
    }
    -- use{'ggandor/flit.nvim',
    -- config=function()
    --     require'flit'.setup{
    --       keys = { f = 'f', F = 'F' },
    --       -- A string like "nv", "nvo", "o", etc.
    --       labeled_modes = "v",
    --       multiline = true,
    --       -- Like `leap`s similar argument (call-specific overrides).
    --       -- E.g.: opts = { equivalence_classes = {} }
    --       opts = {}
    --     }
    -- end}
    use{
       'unblevable/quick-scope',
       config = function()
           vim.g.qs_highlight_on_keys = {'f', 'F'}
       end
    }

    --use"lepture/vim-velocity"
    use'ap/vim-css-color'
    use'junegunn/fzf'
    use'junegunn/fzf.vim'
    use'mileszs/ack.vim'
    use'jeffkreeftmeijer/vim-numbertoggle'
    use'tpope/vim-fugitive'
    use'posva/vim-vue'
    use{'isRuslan/vim-es6'}
    use{'fatih/vim-go'}
    use{
        'ntpeters/vim-better-whitespace',
        config = function()
            vim.g.strip_whitespace_on_save=1
            vim.g.strip_only_modified_lines=1
            vim.g.strip_whitelines_at_eof=1
            vim.g.better_whitespace_guicolor='gray12'
        end
    }
    use'sickill/vim-pasta'
    use'tpope/vim-surround'
    use'chriskempson/base16-vim'
    use {
      'nvim-tree/nvim-tree.lua',
      requires = {
        'nvim-tree/nvim-web-devicons', -- optional, for file icons
      },
      tag = 'nightly', -- optional, updated every week. (see issue #1193)
      config = function()
          require('plugins/tree')
      end
    }
    use 'wbthomason/packer.nvim'
    use "rafamadriz/friendly-snippets"
    use({"L3MON4D3/LuaSnip", tag = "v1.1.0"})
    use {"neovim/nvim-lspconfig"}
    use {
      'VonHeikemen/lsp-zero.nvim',
      requires = {
        -- LSP Support
        {'neovim/nvim-lspconfig'},
        {'williamboman/mason.nvim'},
        {'williamboman/mason-lspconfig.nvim'},

        -- Autocompletion
        {'hrsh7th/nvim-cmp'},
        {'hrsh7th/cmp-buffer'},
        {'hrsh7th/cmp-path'},
        {'saadparwaiz1/cmp_luasnip'},
        {'hrsh7th/cmp-nvim-lsp'},
        {'hrsh7th/cmp-nvim-lua'},

        -- Snippets
        {
            'L3MON4D3/LuaSnip',
            config = function ()
                require('plugins/luasnip')
            end
        },
        {'rafamadriz/friendly-snippets'},
      },
      config = function ()
        require('plugins/lsp-zero')
      end
    }

    use({
        "ray-x/lsp_signature.nvim",
        config = function ()
            require('plugins/lsp_signature')
        end
    })


    use({
        'lewis6991/gitsigns.nvim',
        config = function()
            require('plugins/gitsigns')
        end,
    })

    use {
        'numToStr/Comment.nvim',
        config = function()
            require('Comment').setup()
        end
    }

    use 'jreybert/vimagit'
    use 'junegunn/gv.vim'
    use {
      "folke/trouble.nvim",
      config = function()
        require("trouble").setup {
          -- your configuration comes here
          -- or leave it empty to use the default settings
          -- refer to the configuration section below
        }
      end
    }

    use { 'ggandor/leap.nvim', config = function()
        require('leap').add_default_mappings()
    end }
    -- Markdown viewer
    use({ "iamcco/markdown-preview.nvim", run = "cd app && npm install", setup = function() vim.g.mkdp_filetypes = { "markdown" } end, ft = { "markdown" }, })
    use {'m4xshen/autoclose.nvim',
        config = function ()
            require("autoclose").setup()
        end
    }
    use {"akinsho/toggleterm.nvim", tag = '*', config = function()
      require("toggleterm").setup()
    end}
    use({
      "NTBBloodbath/zig-tools.nvim",
      -- Load zig-tools.nvim only in Zig buffers
      ft = "zig",
      config = function()
        -- Initialize with default config
        require("zig-tools").setup()
      end,
      requires = {
        {
          "akinsho/toggleterm.nvim",
          config = function()
            require("toggleterm").setup()
          end,
        },
        {
          "nvim-lua/plenary.nvim",
          module_pattern = "plenary.*"
        }
      },
    })
end)
-- vim.api.nvim_set_option('completeopt', 'menu')
