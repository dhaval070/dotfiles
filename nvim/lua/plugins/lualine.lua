require('lualine').setup({
    options = {
        theme = "material"
    },
    sections = {
        lualine_b = {'branch'},
        lualine_c = {{
            'filename',
            file_status = true,
            path = 1
        }},
        lualine_x = {},
        lualine_y = {},
        lualine_z = {},
    }
})
