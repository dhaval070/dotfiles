require("luasnip.loaders.from_vscode").lazy_load()

vim.api.nvim_set_keymap('i', '<c-k>', '<cmd>lua require"luasnip".jump(-1)<Cr>', {})
vim.api.nvim_set_keymap('i', '<c-j>', '<cmd>lua require"luasnip".jump(1)<Cr>', {})
