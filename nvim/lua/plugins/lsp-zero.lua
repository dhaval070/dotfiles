local lsp = require('lsp-zero')

lsp.preset('recommended')

lsp.ensure_installed({
  'tsserver',
  'eslint',
--  'sumneko_lua',
  'gopls',
})

lsp.nvim_workspace()

local cmp = require'cmp'
local select_opts = {behavior = cmp.SelectBehavior.Select}
lsp.setup_nvim_cmp({
    mapping = {
        ['<C-n>'] = cmp.mapping.select_next_item(select_opts),
        ['<C-p>'] = cmp.mapping.select_prev_item(select_opts),
        ['<C-e>'] = cmp.mapping.abort(),
        ['<CR>'] = cmp.mapping.confirm({select = false}),
    }
})
lsp.setup()

require('plugins/lsp/servers')
