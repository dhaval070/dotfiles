set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

lua require('options')
lua require('plugins')
lua require('keybindings')
"source ~/.vimrc


colorscheme sonokai
" colorscheme base16-brewer
highlight MatchParen guibg=grey
" colorscheme base16-tomorrow-night
" colorscheme torte
" colorscheme base16-default-dark
" colorscheme base16-ia-dark
" colorscheme base16-material-darker
" colorscheme base16-chalk
" colorscheme base16-seti
